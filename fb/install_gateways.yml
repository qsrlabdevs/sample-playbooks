- name: install http server gateways binaries
  hosts: gateways
  roles:
    - role: nexus_http_server_gw
    - role: nexus_cdr_gateway
    - role: nexus_http_client_gw
    - role: nexus_timer
  tags:
    - gateways

- name: install file cdr gateways instance configuration based on set file_cdr_gw_instances
  hosts: file-cdr
  tasks:
    - name: loop via all instances for this node and install them
      include_role:
        name: nexus_gateway_instance
      vars:
        rtengine_type: filecdr
        rtengine_instance_config_template: "gateway_file_cdrs.json.j2"
        rtengine_name: "{{cdr_instance.instance_name}}"
        gateway_implementation: "nexus-cdr-gateway/nexus-cdr.jar"
        rtengine_config_name: "{{cdr_instance.instance_name}}"
        rtengine_gateway_prometheus_http_port: "{{cdr_instance.prometheus_port}}"
        rtengine_prometheus_metrics_port: "{{cdr_instance.prometheus_port}}"
        rtengine_queue_master: "{{cdr_instance.master_queue}}"
        gateway_repl_port: "{{cdr_instance.repl_port}}"
        gateway_stop_command: stop_cdr_instance
        gateway_restart_command: none
        rtengine_ingress_queue: "{{cdr_instance.ingress_queue_name}}"
        rtengine_config_description: "file cdr gateway - receives requests from service flows and generates cdrs"
        cdr_templates:
            - name: default
              path: "/var/cdr/"
              maxAgeInMinutes: 60
              estimatedCdrLineLength: 1500
              maxNbOfLines: 10000
              closeSuffix: "done"
              prefix: "cdr"
              maxSizeInBytes: 10000000
              openSuffix: "open"
              separatorEscapeSequenceInFieldContent: "'"
              fieldSeparator: ","
              useHeader: False
              useFooter: False
              useLinesCount: False
              useJsonFormat: True
              footer:
                - none
              header:
               - none
              fields:
               - Not_Used
      loop:
        "{{file_cdr_gw_instances}}"
      loop_control:
        loop_var: cdr_instance
  tags:
    - cdrs_instances

- name: install http client gateways instance configuration based on set http_client_gw_instances
  hosts: http-client
  tasks:
    - name: loop via all gateways configured and install them
      include_role:
        name: nexus_gateway_instance
      vars:
        rtengine_instance_config_template: "gateway_http_client.json.j2"
        rtengine_name: "{{http_client_gw_instance.instance_name}}"
        rtengine_config_name: "{{http_client_gw_instance.instance_name}}"
        rtengine_gateway_prometheus_http_port: "{{http_client_gw_instance.prometheus_port}}"
        rtengine_prometheus_metrics_port: "{{http_client_gw_instance.prometheus_port}}"
        gateway_repl_port: "{{http_client_gw_instance.repl_port}}"
        rtengine_queue_master: "{{http_client_gw_instance.master_queue}}"
        rtengine_ingress_queue: "{{http_client_gw_instance.ingress_queue_name}}"
        gateway_implementation: "nexus-http-client/nexus-http-client.jar"
      loop:
        "{{http_client_gw_instances}}"
      loop_control:
        loop_var: http_client_gw_instance
  tags:
    - http_clients_instances

- name: install http server gateways instance configuration 
  hosts: http-server
  tasks:
    - name: loop via all instances for this node and install them
      include_role:   
        name: nexus_gateway_instance
      vars:
        rtengine_instance_config_template: "gateway_http_server.json.j2"
        rtengine_name: "{{http_gw_instance.instance_name}}"
        rtengine_config_name: "{{http_gw_instance.instance_name}}"
        rtengine_gateway_prometheus_http_port: "{{http_gw_instance.prometheus_port}}"
        rtengine_prometheus_metrics_port: "{{http_gw_instance.prometheus_port}}"
        rtengine_queue_master: "{{http_gw_instance.master_queue}}"
        gateway_repl_port: "{{http_gw_instance.repl_port}}"
        gateway_http_server_host: "0.0.0.0"
        gateway_http_server_https_port: "{{http_gw_instance.ssl_http_port}}"
        gateway_http_server_port: "{{http_gw_instance.plain_http_port}}"
        gateway_implementation: "nexus-http-server/nexus-http-server.jar"
        rtengine_gateway_http_egress_queue: "{{http_gw_instance.egress_queue_name}}"
      loop:
        "{{http_server_gw_instances}}"
      loop_control:
        loop_var: http_gw_instance
  tags:
    - httpservers_instances

- name: install timer gateways instances configuration 
  hosts: timers
  tasks:
    - name: loop via all instances for this node and install them
      include_role:   
        name: nexus_gateway_instance
      vars:
        rtengine_instance_config_template: "gateway_timer.json.j2"
        rtengine_name: "{{timer_instance.instance_name}}"
        rtengine_config_name: "{{timer_instance.instance_name}}"
        rtengine_gateway_prometheus_http_port: "{{timer_instance.prometheus_port}}"
        rtengine_prometheus_metrics_port: "{{timer_instance.prometheus_port}}"
        rtengine_queue_master: "{{timer_instance.master_queue}}"
        gateway_repl_port: "{{timer_instance.repl_port}}"
        gateway_implementation: "nexus-timer/nexus-timer.jar"
        gateway_timer_instance_name: "{{timer_instance.timer_name}}"
        gateway_timer_granularity: "{{timer_instance.granularity}}"
        gateway_timer_timers_master: "{{timer_instance.master_timerdb}}"
        rtengine_ingress_queue: "{{timer_instance.ingress_queue_name}}"
      loop:
        "{{timer_gw_instances_set}}"
      loop_control:
        loop_var: timer_instance
  tags:
    - timer_instances


- name: install log gateways instance configuration based on set log_gw_instances
  hosts: log-gw
  tasks:
    - name: loop via all instances for this node and install them
      include_role:
        name: nexus_gateway_instance
      vars:
        rtengine_type: loggw
        rtengine_instance_config_template: "gateway_file_cdrs.json.j2"
        rtengine_name: "{{cdr_instance.instance_name}}"
        gateway_implementation: "nexus-cdr-gateway/nexus-cdr.jar"
        rtengine_config_name: "{{cdr_instance.instance_name}}"
        rtengine_gateway_prometheus_http_port: "{{cdr_instance.prometheus_port}}"
        rtengine_prometheus_metrics_port: "{{cdr_instance.prometheus_port}}"
        rtengine_queue_master: "{{cdr_instance.master_queue}}"
        gateway_repl_port: "{{cdr_instance.repl_port}}"
        gateway_stop_command: stop_cdr_instance
        gateway_restart_command: none
        rtengine_ingress_queue: "{{cdr_instance.ingress_queue_name}}"
        rtengine_config_description: "log gw - receives requests and generates text files for upload to elastic search"
        cdr_templates:
            - name: default
              path: "/var/cdr/"
              maxAgeInMinutes: 60
              estimatedCdrLineLength: 1500
              maxNbOfLines: 10000
              closeSuffix: "done"
              prefix: "cdr"
              maxSizeInBytes: 10000000
              openSuffix: "open"
              separatorEscapeSequenceInFieldContent: "'"
              fieldSeparator: ","
              useHeader: False
              useFooter: False
              useLinesCount: False
              useJsonFormat: True
              footer:
                - none
              header:
               - none
              fields:
               - Not_Used
      loop:
        "{{log_gw_instances}}"
      loop_control:
        loop_var: cdr_instance
  tags:
    - logs_instances