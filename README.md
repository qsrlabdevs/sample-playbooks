
# TL;DR

Here is the sequence of commands I use to install everything: 

```bash
#this is the only playbook to use root user; it creates regular OS user and installs java 8 with YUM (optional)
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method sudo -b -i inventory/hosts.yaml base_setup.yml 
#now we use ocvas user (regular OS)
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_aep_hosts.yml 
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml create_certificates.yml 
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml copy_certificates.yml 
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_redis_db_instances.yml
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_gateways.yml  
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_aep_oauth2.yml
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_aep_web_be.yml
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_aep_web_fe.yml
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_observability_apps.yml 
ansible-playbook --private-key ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method su --become-user ocvas -b -K -i inventory/hosts.yaml install_rtengines.yml 
```

For TCAP GW as traffic generator and VSSF simulator, here are the commands:

```bash
#this installs tcapgw and tcapgw instances for traffic generator; must be run as root (or with sudo) because it tries to add ocadmin group to OS install user 
ansible-playbook --private-key  ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method sudo -b -i inventory/hosts.yaml tcapgw_gen.yml
#this installs tcapgw and tcapgw instances for VSSF traffic simulator; must be run as root (or with sudo) because it tries to add ocadmin group to OS install user 
ansible-playbook --private-key  ~/dev/gcloud/gcloud1.key -u florin_va_oltean_gmail_com --become-method sudo -b -i inventory/hosts.yaml tcapgw_vssf.yml
```

>> *important* : we don't have a sample for installing simple trap sender; however this is easy to do by yourself
# Main changes since last commit

## 12.07.2022

> read readme files of updated roles; there are new variables

1. update versions 

## 14.06.2022

1. updated role versions (rte instance, grafana and prometheus mainly)
2. add prometheus alert manager role in observability playbook and inventory

## 24.05.2022

1. update tcapgw version in roles list
2. correct playbooks for tcapgw_gen and tcapgw_vssf

1. added more roles into playbooks `install_rtengine.yml` and `install_aep_web_be.yml`
2. update roles list with new versions 

## 23.05.2022

1. added host groups for traffic generator (SS7) and vssf simulator (SS7) with corresponding group vars and playbooks

## 13.05.2022

1. added log gateway to produce logs for splunk
2. also corrected the suffix and prefix value of cdr parameters

## 09.05.2022


1. split **web_app** into three distinct components: oauth2, web backend, web frontend. The reason is to allow a better network deployment layout. 
2. this means we have 3 new playbooks (for fe install, for be install and for oauth2 server/keycloak): `install_aep_web_fe.yml`, `install_aep_web_be.yml`, `install_aep_oauth2.yml`
3. also, the inventory has 3 more groups under web_apps: `web_fe`, `web_be`, `oauth2`
4. **IMPORTANT**: web front-end installs the static web apps and produces a configuration for nginx. but it does not install nginx server itself.

## Earlier
1. split server and playbooks between **observability** and **web_app** (both were previously included under **management** group)
2. included central monitoring instance (a rtengine acting as SNMP gateway - traps and counters) 
3. updated voltdb config to use and array of hosts 
4. moved voltdb gateway into group **gateways**

# Instructions 

1. make sure you have all packages required 
2. map out the hosts and build deployment "diagram"
3. define parameters for your redis dbs 
4. define parameters for your real time engines
5. define parameters for your gateways
6. define parameters for your management apps (including AEP GUI)
7. cross your fingers... and start

> We use Ansible; I remind you that there are 3 main concepts as far as I am concerned:

   * roles: we deliver these; they are parametrized receipts to install or configure our software components. 
   * inventory: layout of your hosts/containers including parameters depending on your architecture. 
   * playbooks: they glue together roles and inventory; describes where to install what. They are orchestrators essentially laying out what is to be done first (and where), what is to be done next, and so on


> You should know Ansible. 

> It is expected to modify the inventory as well as the playbooks. They are just samples.

## Known problems

None; but if you find one, we fix it immediately!

## Result & Tips

At the end of the execution of commands, you should have a working environment (of course, change before inventory)

You should be able to access on http port 5000 (unless you change it) on every server the console of monit daemon (and start / stop processes, check status, etc); for now, access is unencrypted and uses the famous last words ```admin/admin```

Here is what files I changed after wipping out entire environment, recreating virtual servers with new IPs:

1. inventory/group_vars/all/20_certificates.yml (for adjusting the certificate SAN to correct IPs and DNS names of new environment; include all public IPs of http servers gateways and also of the gui)
2. inventory/group_vars/all/25_redis_base.yml (for changing IP addresses for redis sentinels and cluster)
3. inventory/group_vars/all_redis_servers/databases.yml (for changing IP addresses for redis)
4. inventory/group_vars/web_app/0_conectivity_hosts_ports.yml (for changing IP addresses)
5. inventory/group_vars/observability/0_conectivity_hosts_ports.yml (for changing IP addresses)
6. inventory/group_vars/all/rtengine.yml, you should change the variable ```rtengine_config_template_folder: "/Users/me/qsrlabs/roles/templates"``` to point to the location on your ansible controller where config templates are stored; this is typically in ```roles/templates``` under the location where you expanded the tarballs.

## Environment

This is tested in GCP; therefore each of managed hosts has two addresses: one public (dynamically allocated when we start servers) and one private (stable across restarts). This is why you see public hosts variable in inventory. 

Also, like in AWS, also in GCP we connect with ssh using a private key; and then we run all tasks as "ocvas". This is why we have this syntax of calling ansible:

```ansible-playbook --private-key <file> -u <user_to_connect> --become-method su --become-user <user-to-run-tasks-as> -b -K```
# Preparations
## Do you have all packages?

Please consult the file [roles-list/proximus-versions.md]; you must download and expand all files based on the versions indicated. 

The packages are in Quasar AWS S3 bucket called "nexus-roles"; you should have access to it based on digital certificates. 

Use same location to download all files starting with "ansible_" from "nexus-roles" S3 bucket.

## Do you know the servers layout? 

This sample configuration assumes you split the infrastructure in broadly 6 categories:
   - redis data nodes (sentinels, masters, replicas and clusters)
   - observability  nodes (Prometheus, Grafana, custom SNMP gateway)
   - web app nodes (Keycloak, AEP GUI front end and backend)
   - gateways (nodes where we run gateways such as ldap gw or http server or client gw); gateways themselves are further split per types (http servers, http clients, cdr gateways, etc. 
   - rtengines ; also we may have multiple types of rtengines. 
   - certificate store (it should be only one server, usually it is the same as management node)

> The above is just a suggestion; you may choose any split. Remember that same host may be part of several groups as it may host different processes. 

## Redis instances

We use redis for two purposes (but none of them as a cache):

1. as a message bus / message queue 
2. as a data store 

> Nothing works if redis is not properly configured!

Therefore, it is very important to understand where and how you install redis; we have several redis instances types:

1. redis instance used as monitoring db (in this instance all running processes are recording their state), or as a tracing database (we store traces here, etc). It is a mandatory db, it must be configured in redis sentinel setup.  
2. redis instance used as queue system; also it must be currently configured in sentinel setup
3. redis instance used as configuration store; it may be collocated with one of the previous dbs. 
4. redis instance(s) for sessions and session queues; it can be a sentinel based configuration or a cluster based configuration
5. there may be more redis data stores for various purposes. 

Parameters like replication, timeouts, bind address, ports, etc should be determined by you. 

Use file *inventory/group_vars/all_redis_servers/all_redis_instances.yml* to adjust and define/remove redis instances, sentinels and clusters. 

## Real time engines (rtengines)

You must decide if you collocated all your processing flows on the same instances of rtengines or you set up different rtengines instances for different application/services (it can be beneficial for operations purposes). 

Use file *inventory/group_vars/rtegines/instances.yml*  to define your instances. 

You can also alter file *install_rtengines.yml* to create new type of engines, if required. 

## Gateways 

These are components that allows communication with external environment; they are essentially protocol convertors but also have elaborate logic for failover, overload protection, degraded mode, etc. 

Use file *inventory/group_vars/gateways/instances.yml*  to define your instances. 

You can also alter file *install_gateways.yml* to create new type of gateways, if required. 

## Certificates

AEP solution uses certificates in several places/scenarii:

1. server side certificate for AEP GUI and for Keycloak/Oauth2 server
2. server side certificate for Nexus AEP Http Server Gateway 
3. if we connect to external systems (like Bitbucket, GitLab, etc) and these are using customer generated certificates, we need the Root CA used (along with the whole CA chain) to be added into our platform truststore. 
4. if the customer uses mutual TLS for connecting to our https server gateway, we need the root CA or the client certificates to be included into our platform truststore

Normally, the customer should provide the above set of certificates to use; we need customer to provide at least:

1. server side certificate to be used by web app (AEP GUI), Keycloak (OAuth2 server) and HTTP Server GW and password

   - the certificate should be in JKS format (Java KeyStore) or PKCS12 (standard format). 
   - a public/private keys pair (X.509 format, usually the files have pem extension); it is possible to generate PKCS12 certificate from these two 

However, we also provide two roles and associated playbooks to generate self signed certificate; in case you are using these playbooks, please 
look at file ```inventory/group_vars/all/20_certificates.yml``` and change according to your environmet; especially ***subject_alternative_names***.

## Web app

From my point of view, this is the most complicated installation, because of SSL certificates and IP addresses (internal and external). But:

1. you need to look into file ```0_connectivity_hosts_ports.yml``` and:

   - *sce_be_public_host_ip*: is the ip address you use in your web browser to connect to AEP GUI
   - *sce_be_public_host_port*: it is the https port 
   - *oauth2_public_host_ip*: is the address where keycloak (OAuth2 server) binds; you can actualy put "0.0.0.0"; in my case, because of NAT, public is different than IP available on the host itself; but maybe in your environment is the same. Anyway, bind to all interfaces is a safe bet to start with. 
   - *oauth2_public_host_port*: https port of OAuth2 server
   - *sce_fe_public_host_ip*: front end web server ip
   - *sce_fe_public_host_port*: front end web server https port

2. you should modify file ```17_frontend.yml``` to provide for external app meaningfull settings for your environment; for each of your server, insert he link with address you may use from your browser to connect to port 5000 where monit daemon exposes the web interface; it will allow you to manage processes via web. 

## Observability

This is about installing grafana, prometheus and a special rtengine instance that is configured to collect all metrics and expose them as SNMP; also it sends alarms. 

1. you need to look into file ```0_connectivity_hosts_ports.yml``` and change IP addresses:
   - grafana_public_facing_domain_name: IP/host you use to access grafana UI from your browser
   - also the listen address for prometheus server


## TCAP GW

We provide playbooks to install tcap gw; there are some particularities:

1. they must be run on hosts where HPE USPM runtime is installed 
2. the playbooks run as root (or sudo) because we must add group "ocadmin" to install OS user (typically ocvas)
3. we provide two roles: `tcapgw` that installs the binaries, and `tcapgw-install` that install config, monit, etc. 
4. we provide two playbooks, but really housekeeping: to install a traffic generator (`tcapgw_gen.yml`) and to install VSSF simulator (`tcapgw_vssf.yml`)






