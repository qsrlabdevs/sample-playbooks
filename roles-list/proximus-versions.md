
| package and version | role name |
|---------------------|-----------|
|nexus-rtengine-release-v2.2.5-1| binaries for real time engines|
|nexus-rte-instance-role-release-v4.1.0-1|foundation role to configure rtengine|
|nexus-cdr-gateway-release-v5.2.2-3|nexus_cdr_gateway|
|nexus-cdr-gateway-release-v5.2.2-3| nexus_cdr_gateway_rte 
|nexus-cdr-gateway-release-v5.2.2-3| nexus_cdr_gateway_gui 
|nexus-gateway-instance-release-v3.3.0-1| nexus_gateway_instance configuration 
|redis-http-gw-release-v3.1.2-1| nexus_http_client_gw 
|redis-http-gw-release-v3.1.2-1| nexus_http_client_gw_rte 
|redis-http-gw-release-v3.1.2-1| nexus_http_client_gw_gui 
|nexus-certs-release-v1.0.0-1| nexus_certs
|nexus-http-server-release-v4.8.1-2| nexus_http_server_gw
|nexus-http-server-release-v4.8.1-2| nexus_http_server_gw_rte 
|nexus-http-server-release-v4.8.1-2| nexus_http_server_gw_gui
|nexus-rta-release-1.2.15-1| nexus_voice_ss7_antifraud 
|nexus-rta-release-1.2.15-1| nexus_voice_ss7_antifraud_gui
|nexus-sms-release-v1.1.45-2| nexus_ss7_sms_processing 
|nexus-sms-release-v1.1.45-2| nexus_ss7_sms_gui 
|nexus-sip-release-v2.1.8-1| nexus_sip_rte 
|nexus-sip-release-v2.1.8-1| nexus_sip_gui 
|nexus-redis-tracer-release-v1.2.1-1| nexus_redis_tracer
|nexus-prometheus-release-v2.0.1-1| nexus_prometheus 
|nexus-grafana-release-v1.1.0-1| nexus_grafana 
|nexus-keycloak-release-v10.0.2-14| nexus_keycloak
|nexus-gui-backend-release-v2.3.4-2| nexus_gui_backend
|nexus-occp-cap-release-v1.1.15-1| nexus_occptcap_rte 
|nexus-occp-cap-release-v1.1.15-1| nexus_occptcap_gui
|nexus-monit-role-release-v3.3.3-1| nexus_monit 
|nexus-package-release-v3.3.2-1| nexus_redis 
|nexus-package-release-v3.3.2-1| nexus_redis_data_node 
|nexus-package-release-v3.3.2-1| nexus_redis_sentinel 
|nexus-package-release-v3.3.2-1| nexus_redis_cluster_start 
|nexus-package-release-v3.3.2-1| nexus_base_setup 
|nexus-package-release-v3.3.2-1| nexus_redis_servers_base_systemd 
|nexus-voltdbgw-release-v1.0.1-1| nexus_voltdbgw
|nexus-sce-release-v2.4.3-3| nexus_aep_gui_studio
|nexus-script-editor-release-v1.3.0-2| nexus_aep_scripting_studio
|nexus-gui-floweditor-release-v2.2.0-2| nexus_aep_flow_editor
|nexus-security-release-v1.0.0-4| nexus_security_rte 
|nexus-security-release-v1.0.0-4| nexus_security_gui 
|nexus-timers-release-v4.2.1-1 | nexus_timer
|nexus-timers-release-v4.2.1-1 | nexus_timer_rte
|nexus-timers-release-v4.2.1-1 | nexus_timer_gui
|~~nexus-enumgw~~ | under implementation of automatic deployment
| HPE vTAS HOSTED - they are not deployed automatically anylonger, not tested for this
|capgw-occp-release-v1.8-1| nexus_tas_capservice 
|capgw-occp-release-v1.8-1| nexus_capgwoccp_conf 
|sipgw-occp-release-v1.20-1| nexus_tas_deps 
|sipgw-occp-release-v1.20-1| nexus_tas_sipservice 
|sipgw-occp-release-v1.20-1| nexus_sipgwoccp_conf 
|nexus_tas_map_service (mapgw-tas)
|nexus_mapgwoccp_conf (mapgw-tas)
| HPE USPM HOSTED - not tested
|tcapgw_release-v2.1.0-1| tcapgw
|tcapgw_release-v2.1.0-1| tcapgw-instance 

